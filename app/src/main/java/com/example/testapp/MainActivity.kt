package com.example.testapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        println("hello m8")

        val button1 = findViewById<Button>(R.id.button1)
        val result = findViewById<TextView>(R.id.textView2)
        val get_result = findViewById<TextView>(R.id.textView)

        button1.setOnClickListener {
            val rand = (0..10).random()
            result.text = rand.toString()
            get_result.text = getString(R.string.display1)
        }
    }
}
